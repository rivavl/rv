package com.marina.navigation.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.activity.addCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.marina.navigation.R
import com.marina.navigation.ShowAndHideBottomNavBar
import com.marina.navigation.databinding.FragmentWebBinding
import com.marina.navigation.util.SH_PREFS
import com.marina.navigation.util.USER_URL

class WebFragment : Fragment(R.layout.fragment_web) {

    private var _binding: FragmentWebBinding? = null
    private val binding: FragmentWebBinding
        get() = _binding ?: throw RuntimeException("FragmentWebBinding == null")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWebBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //показываем BottomNavigationView
        (requireActivity() as ShowAndHideBottomNavBar).showBottomView()
        webViewSetup()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //переопределяем кнопку назад
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            //если есть что-то в истории, идем назад
            if (binding.webView.canGoBack()) {
                binding.webView.goBack()
                //если нет, но юрл указывает не на гугл, загружаем поиск
            } else if (binding.webView.url != "https://www.google.ru/") {
                binding.webView.loadUrl("https://www.google.ru/")
            } else {
                //иначе выполняем то, что указано в активити
                requireActivity().onBackPressed()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //сохраняем последний юрл перед выходом
        binding.webView.url?.let { saveUserUrl(it) }
        _binding = null
    }


    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetJavaScriptEnabled")
    private fun webViewSetup() {
        binding.webView.webViewClient = WebViewClient()
        val userUrl = getUserUrl()
        binding.webView.apply {
            if (userUrl.isNotEmpty()) {
                loadUrl(userUrl)
            } else {
                loadUrl("https://www.google.ru/")
            }
            settings.javaScriptEnabled = true
            settings.safeBrowsingEnabled = true
        }
    }

    //сохранение
    @SuppressLint("CommitPrefEdits")
    private fun saveUserUrl(url: String) {
        val sharedPrefs = activity?.getSharedPreferences(SH_PREFS, Context.MODE_PRIVATE)
        val editor = sharedPrefs?.edit()
        editor?.apply {
            putString(USER_URL, url)
            apply()
        }
    }

    //восстановление
    private fun getUserUrl(): String {
        val sharedPrefs = activity?.getSharedPreferences(SH_PREFS, Context.MODE_PRIVATE)
        return sharedPrefs?.getString(USER_URL, "").toString()
    }


    companion object {
        fun newInstance() = WebFragment()
    }
}
