package com.marina.navigation.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.marina.navigation.HomeFragmentViewModel
import com.marina.navigation.R
import com.marina.navigation.ShowAndHideBottomNavBar
import com.marina.navigation.StringAdapter
import com.marina.navigation.databinding.FragmentHomeBinding

class HomeFragment : Fragment(R.layout.fragment_home) {

    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding
        get() = _binding ?: throw RuntimeException("FragmentHomeBinding == null")
    private lateinit var stringAdapter: StringAdapter
    private lateinit var viewModel: HomeFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[HomeFragmentViewModel::class.java]
        setupRecyclerView()
        (requireActivity() as ShowAndHideBottomNavBar).showBottomView()
        viewModel.list.observe(requireActivity()) {
            Log.d(this.javaClass.simpleName, "${viewModel.list.value}")
            stringAdapter.submitList(it)
        }
    }

    private fun setupRecyclerView() {
        setupAdapter()
        setupOnClickListener()
    }

    //создаем адаптер и присваиваем его в качестве адаптера ресайклера
    private fun setupAdapter() {
        stringAdapter = StringAdapter()
        binding.rvStringList.adapter = stringAdapter
    }

    //обработка нажатия на вью в ресайклере
    private fun setupOnClickListener() {
        stringAdapter.onStringItemClickListener = {
            viewModel.changeStringIsChanging(it)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun newInstance() = HomeFragment()
    }

}
