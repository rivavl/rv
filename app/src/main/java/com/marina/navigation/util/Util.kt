package com.marina.navigation.util

import com.marina.navigation.R
import com.marina.navigation.model.StringItem
import kotlin.random.Random

//массив со списком стилей, заданных в themes
object Style {
    val STYLES = listOf<Int>(
        R.style.myStyle1,
        R.style.myStyle2,
        R.style.myStyle3,
        R.style.myStyle4,
        R.style.myStyle5,
        R.style.myStyle6,
        R.style.myStyle7,
        R.style.myStyle8,
        R.style.myStyle9,
        R.style.myStyle10,
        R.style.myStyle11,
        R.style.myStyle12,
        R.style.myStyle13,
        R.style.myStyle14,
    )

    //получение рандомного стиля
    fun getRandomStyle(): Int {
        return STYLES[Math.abs(Random.nextInt() % STYLES.size)]
    }
}

//список строк
val LIST = listOf(
    StringItem("String1", true, 0),
    StringItem("String2", true, 1),
    StringItem("String3", false, 2),
    StringItem("String4", false, 3),
    StringItem("String5", false, 4),
    StringItem("String6", false, 5),
    StringItem("String7", false, 6),
    StringItem("String8", false, 7),
    StringItem("String9", false, 8),
    StringItem("String10", false, 9),
    StringItem("String11", false, 10),
    StringItem("String12", false, 11),
)

const val USER_URL = "user_url"
const val SH_PREFS = "sh_prefs"