package com.marina.navigation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.view.get
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.marina.navigation.model.StringItem


class StringAdapter :
    ListAdapter<StringItem, StringAdapter.StringItemViewHolder>(StringItemDiffCallback()) {

    var onStringItemClickListener: ((StringItem) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringItemViewHolder {
        val layout = R.layout.rv_item
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return StringItemViewHolder(view)
    }


    override fun onBindViewHolder(holder: StringItemViewHolder, position: Int) {
        //берем элемент
        val stringItem = getItem(position)
        //берем кардвью
        val card = holder.view as CardView
        //первый элемент кардвью (он же единственный)
        val tv = card[0] as TextView
        //назначаем стиль такствью
        tv.setTextAppearance(stringItem.currentStyle)
        //назначаем текст
        holder.tvContent.text = stringItem.string
        //обработка нажатия на вью
        holder.view.setOnClickListener {
            onStringItemClickListener?.invoke(stringItem)
        }
    }

    class StringItemDiffCallback : DiffUtil.ItemCallback<StringItem>() {

        override fun areItemsTheSame(oldItem: StringItem, newItem: StringItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: StringItem, newItem: StringItem): Boolean {
            return oldItem == newItem
        }
    }

    class StringItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvContent = view.findViewById<TextView>(R.id.str_in_rv)
    }


}


