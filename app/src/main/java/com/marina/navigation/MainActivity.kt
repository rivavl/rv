package com.marina.navigation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.marina.navigation.databinding.ActivityMainBinding
import com.marina.navigation.fragments.HomeFragment
import com.marina.navigation.fragments.WebFragment

class MainActivity : AppCompatActivity(), ShowAndHideBottomNavBar {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupBottomNav()
    }

    private fun setupBottomNav() {
        val bottomNavigationView = binding.bNav
        bottomNavigationView.menu.findItem(R.id.home).isChecked = true
        supportActionBar?.title = getString(R.string.home).uppercase()
        binding.bNav.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.home -> {
                    setTitleActionBar(R.string.home)
                    setFragment(HomeFragment.newInstance())
                    true
                }
                R.id.web -> {
                    setTitleActionBar(R.string.web)
                    setFragment(WebFragment.newInstance())
                    true
                }
                else -> false
            }
        }
    }

    //установка названия фрагмента в toolbar
    private fun setTitleActionBar(id: Int) {
        supportActionBar?.title = getString(id).uppercase()
    }

    //установка фрагмента
    private fun setFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.commit()
    }

    override fun hideBottomView() {
        binding.bNav.visibility = View.GONE
    }

    override fun showBottomView() {
        binding.bNav.visibility = View.VISIBLE
    }
}

interface ShowAndHideBottomNavBar {
    fun hideBottomView()
    fun showBottomView()
}
