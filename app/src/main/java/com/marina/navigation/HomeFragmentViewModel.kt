package com.marina.navigation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.marina.navigation.model.StringItem
import com.marina.navigation.util.LIST
import com.marina.navigation.util.Style
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class HomeFragmentViewModel : ViewModel() {

    private lateinit var job: Job
    private val _list = MutableLiveData(LIST)
    val list: LiveData<List<StringItem>> get() = _list

    init {
        //первым делом начинаем обновлять стили
        //(только тех элементов, у которых isChanging == true)
        startUpdate()
    }

    override fun onCleared() {
        super.onCleared()
        //перестаем обновлять стили
        stopUpdate()
    }

    //устанавливаем рандомные значения стилей
    //с задержкой 2 секунды
    private fun startUpdate() {
        job = viewModelScope.launch {
            while (true) {
                delay(2000L)
                for (i in 0 until _list.value?.size!!) {
                    if (_list.value!![i].isChanging) {
                        setRandomStyle(i)
                    }
                }
            }
        }
        job.start()
    }

    //изменение свойства isChanging у элемента
    fun changeStringIsChanging(stringItem: StringItem) {
        //создаем новый элемент с противоположным свойством isChanging
        //остальные не меняются
        val newItem = stringItem.copy(isChanging = !stringItem.isChanging)
        val newList = _list.value?.toMutableList()
        newList?.removeAt(stringItem.id)
        newList?.add(newItem.id, newItem)
        _list.value = newList
        Log.d(this.javaClass.simpleName, "${_list.value}")
    }

    private fun stopUpdate() {
        job.cancel()
    }

    private fun setRandomStyle(id: Int) {
        val item = _list.value?.get(id)
        val newItem = item?.copy(currentStyle = Style.getRandomStyle())
        val newList = _list.value?.toMutableList()
        item?.id?.let { newList?.removeAt(it) }
        newItem?.id?.let { newList?.add(it, newItem) }
        _list.value = newList
    }
}
