package com.marina.navigation.model

import com.marina.navigation.util.Style

//строка
data class StringItem(
    //текст
    val string: String,
    //меняются стили или нет
    var isChanging: Boolean = false,
    val id: Int = UNDEFINED_ID,
    //текущий стиль
    var currentStyle: Int = Style.STYLES[0]
) {
    companion object {
        const val UNDEFINED_ID = -1
    }
}
